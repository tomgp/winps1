# Install PowerShell modules:
# Install-Module -Name Terminal-Icons -Repository PSGallery
# Install-Module -Name posh-git -Scope CurrentUser -Force -Repository PSGallery

oh-my-posh init pwsh | Invoke-Expression

$env:POSH_GIT_ENABLED = $true
Import-Module -Name Terminal-Icons

function isAdmin {
    [Security.Principal.WindowsIdentity]::GetCurrent().Groups -contains 'S-1-5-32-544'
}

function requireAdmin {
    if (!(isAdmin)) {
        Write-Warning "You are not admin in this terminal! Skipping..."
        return $false
    }

    $true
}

function requireUser {
    if (isAdmin) {
        Write-Warning "You are an admin in this terminal! Skipping..."
        return $false
    }

    $true
}
    
function doWinHealth {
    if (!(requireAdmin)) { return }

    Write-Output ''
    Write-Output 'To see what''s happening with the Dism/sfc commands:'
    Write-Output 'Get-Content -Wait $env:windir\Logs\CBS\CBS.log'
    Write-Output 'Get-Content -Wait $env:windir\Logs\DISM\dism.log'
    Write-Output ''
    Dism.exe /Online /Cleanup-Image /ScanHealth
    # add /Source:C:\RepairSource\Windows /LimitAccess to dism to use another windows installation as source for repair
    Dism.exe /Online /Cleanup-Image /RestoreHealth
    sfc.exe /scannow
    # bleachbit_console.exe --preset -c
    cleanmgr.exe /sagerun:0 # To configure this, do first as an admin: cleanmgr.exe /sageset:0
    Write-Output ''
    Write-Output 'Other useful commands: powercfg {-energy|-waketimers|-requests|-sleepstudy|-a}, systeminfo, dotnet --info, perfmon {/res|/rel|/report}, msconfig, control, msinfo32'
    Write-Output ''
    Write-Output 'To use Windows Performance Analyzer to show DPC / ISR stats:'
    Write-Output 'xperf -on latency -stackwalk profile; Start-Sleep -Seconds 60; xperf -stop -d c:\temp\trace.etl'
    Write-Output 'To get a txt report from the etl trace: xperf -i c:\temp\trace.etl -o c:\temp\report.txt -a dpcisr'
    Write-Output ''
    Write-Output 'To check the disk: chkdsk.exe C: /offlinescanandfix'
}

function doWinClean {
    if (!(requireAdmin)) { return }

    Dism.exe /online /Cleanup-Image /AnalyzeComponentStore
    Dism.exe /online /Cleanup-Image /StartComponentCleanup /ResetBase
    Dism.exe /online /Cleanup-Image /SPSuperseded
}

function doNetStackReset {
    if (!(requireAdmin)) { return }
        
    ipconfig /release
    ipconfig /flushdns
    ipconfig /renew
    Write-Output 'Other commands you could use: netsh int ip reset, netsh int ip install, netsh winsock reset'
}

function doUpgrade {
    if (!(requireUser)) { return }

    UsoClient ScanInstallWait
    Update-Module -Name Terminal-Icons -Force
    Update-Module -Name posh-git -Scope CurrentUser -Force
    winget upgrade --all -i
    explorer ms-windows-store:updates
    explorer ms-settings:windowsupdate
}

Set-Alias syshealth doWinHealth
Set-Alias syshealth-extra doWinClean
Set-Alias netstackreset doNetStackReset
Set-Alias sysupgrade doUpgrade
Set-Alias sysupdate doUpgrade